package clases;

import java.util.Scanner;

/**
 * clase Gimnasio que recibe los vectores de clase Cliente y Productos para
 * realizar los metodos
 * 
 * @author PATRICIA.ALCALA.ALCOBER
 *
 */

public class Gimnasio {

	static Scanner teclado = new Scanner(System.in);
	/**
	 * Creacion de variables par contar
	 */
	private static int numAlta;
	private static int numBaja;

	/**
	 * Creacion de los vectores para rellenarlos a continuacion
	 */
	private Cliente[] clientes;
	private Productos[] productos;;

	/**
	 * 
	 * @param tamanno para dar longitud a los vectores al inicializarlos
	 */
	public Gimnasio(int tamanno) {
		this.clientes = new Cliente[tamanno];
		this.productos = new Productos[tamanno];
	}

	/**
	 * pedir los datos al cliente para interactuar con el contar cuantos se dan de
	 * alta
	 */
	public void altaCliente() {

		for (int i = 0; i < clientes.length; i++) {
			if (clientes[i] == null) {
				clientes[i] = new Cliente();

				System.out.println("Por favor, introduzca su nombre");
				String nombre = teclado.nextLine();
				clientes[i].setNombre(nombre);

				System.out.println("Introduzca su apellido");
				String apellido = teclado.nextLine();
				clientes[i].setApellido(apellido);

				System.out.println("Introduzca su DNI");
				String dni = teclado.nextLine();
				clientes[i].setDni(dni);

				if ((dni.length() == 9) && (dni.charAt(0) >= '0' && dni.charAt(0) <= '9')
						&& (dni.charAt(1) >= '0' && dni.charAt(1) <= '9')
						&& (dni.charAt(2) >= '0' && dni.charAt(2) <= '9')
						&& (dni.charAt(3) >= '0' && dni.charAt(3) <= '9')
						&& (dni.charAt(4) >= '0' && dni.charAt(4) <= '9')
						&& (dni.charAt(5) >= '0' && dni.charAt(5) <= '9')
						&& (dni.charAt(6) >= '0' && dni.charAt(6) <= '9')
						&& (dni.charAt(7) >= '0' && dni.charAt(7) <= '9')
						&& (dni.charAt(8) >= 65 && dni.charAt(8) <= 90)) {
					System.out.println("DNI admitido");
				} else {
					do {
						System.out.println("DNI err�neo, por favor vuelva a introducirlo");
						dni = teclado.nextLine();
						clientes[i].setDni(dni);
					} while (dni.length() != 9);
					System.out.println("DNI admitido");
				}

				System.out.println("Por �ltimo, introduzca su edad");
				int edad = teclado.nextInt();
				clientes[i].setEdad(edad);
				teclado.nextLine();
				int numsocio = (int) (Math.random() * 700) + 1;
				clientes[i].setNumsocio(numsocio);
				System.out.println("Numero de socio asignado: " + clientes[i].getNumsocio());
				System.out.println();
				System.out.println("Sus datos han sido metidos exitosamente y son:");
				System.out.println(clientes[i]);

				numAlta++;

				break;
			}
		}
		System.out.println("Se han dado de alta " + numAlta + " cliente(s)");

	}

	/**
	 * metodo que busca un cliente mediante su numero de socio
	 * 
	 * @param numsocio para poder filtrar
	 * @return datos del socio si coincide el numero de socio, sino es null
	 */
	public Cliente buscarCliente(int numsocio) {
		for (int i = 0; i < clientes.length; i++) {
			if (clientes[i] != null) {
				if (clientes[i].getNumsocio() == numsocio) {
					return clientes[i];
				}
			}
		}
		return null;
	}

	/**
	 * metodo que elimina un cliente mediante su numero de socio
	 * 
	 * @param numsocio para eliminar un cliente contar los que se dan de baja
	 */
	public void eliminarCliente(int numsocio) {
		for (int i = 0; i < clientes.length; i++) {
			if (clientes[i] != null) {
				if (clientes[i].getNumsocio() == numsocio) {
					clientes[i] = null;
					numBaja++;

				}
			}
		}
		System.out.println("Cliente eliminado exitosamente");
		System.out.println("Se han dado de baja " + numBaja + " cliente(s)");
	}

	/**
	 * listar todos clientes, si no hay saldr� la frase indicada
	 */
	public void listaGeneral() {
		for (int i = 0; i < clientes.length; i++) {
			if (clientes[i] != null) {
				System.out.println(clientes[i]);
			} else if (clientes[0] == null) {
				System.out.println("�Lo sentimos! En estos momentos no figura ning�n cliente en nuestra base");
				break;
			}

		}

	}

	/**
	 * metodo que permite cambiar el dni si el numero de socio es correcto
	 * 
	 * @param nums1 para pasar el numero de socio pedido en el programa principal
	 * @param dni2  para cambiar el dni
	 */
	public void cambiarDatosCliente(int nums1, String dni2) {
		for (int i = 0; i < clientes.length; i++) {
			if (clientes[i] != null) {
				if (clientes[i].getNumsocio() == nums1) {
					clientes[i].setDni(dni2);
				}
			}
		}
	}

	/**
	 * metodo que permite cambiar la edad si el numero de socio es correcto
	 * 
	 * @param nums2 para pasar el numero de socio pedido en el programa principal
	 * @param edad2 para cambiar la edad
	 */
	public void cambiarDatosCliente(int nums2, int edad2) {
		for (int i = 0; i < clientes.length; i++) {
			if (clientes[i] != null) {
				if (clientes[i].getNumsocio() == nums2) {
					clientes[i].setEdad(edad2);
				}
			}
		}
	}

	/**
	 * metodo que permite cambiar el dni y edad si el numero de socio es correcto
	 * 
	 * @param nums3 para pasar el numero de socio pedido en el programa principal
	 * @param dni3  para cambiar el dni
	 * @param edad3 para cambiar la edad
	 */
	public void cambiarDatosCliente(int nums3, String dni3, int edad3) {
		for (int i = 0; i < clientes.length; i++) {
			if (clientes[i] != null) {
				if (clientes[i].getNumsocio() == nums3) {
					clientes[i].setDni(dni3);
					clientes[i].setEdad(edad3);
				}
			}
		}
	}

	/**
	 * interactuar con el cliente pidiendole sugerencias
	 */
	public void sugerencias() {
		for (int i = 0; i < productos.length; i++) {
			if (productos[i] == null) {
				productos[i] = new Productos();
				System.out.println("Por favor escriba el nombre del producto");
				String nproducto = teclado.nextLine();
				productos[i].setNombre(nproducto);
				System.out.println("Escriba el tipo de producto");
				String tproducto = teclado.nextLine();
				productos[i].setTipo(tproducto);
				System.out
						.println("El producto escogido es: " + productos[i].getNombre() + " " + productos[i].getTipo());
				System.out.println("�Est� seguro de su decisi�n? Si/No");
				String respuesta = teclado.nextLine();
				if (respuesta.equalsIgnoreCase("si")) {
					System.out.println("Muchas gracias, intentaremos conseguir el producto lo antes posible");
				} else if (respuesta.equalsIgnoreCase("no")) {
					System.out.println("�Qu� desea cambiar? (Nombre/Tipo)");
					String respuesta2 = teclado.nextLine();
					if (respuesta2.equalsIgnoreCase("nombre")) {
						System.out.println("Escriba de nuevo el nombre");
						String nproducto2 = teclado.nextLine();
						productos[i].setNombre(nproducto2);
						System.out.println("Muchas gracias, intentaremos conseguir " + productos[i].getNombre() + " "
								+ productos[i].getTipo() + " lo antes posible");
					} else if (respuesta2.equalsIgnoreCase("tipo")) {
						System.out.println("Escriba de nuevo el tipo");
						String tproducto2 = teclado.nextLine();
						productos[i].setTipo(tproducto2);
						System.out.println("Muchas gracias, intentaremos conseguir " + productos[i].getNombre() + " "
								+ productos[i].getTipo() + " lo antes posible");
					}
				}
				break;
			}
		}
	}
}