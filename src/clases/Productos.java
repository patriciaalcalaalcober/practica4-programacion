package clases;

/**
 * clase Productos con sus atributos, constructor, getter y setter y toString
 * 
 * @author PATRICIA.ALCALA.ALCOBER
 *
 */

public class Productos {
	/**
	 * atributos
	 */
	private String nombre;
	private String tipo;
	private double peso;
	private double precio;
	private int existencias;

	/**
	 * constructor con parametros para rellenarlo como quiera
	 * 
	 * @param nombre      del producto
	 * @param tipo        del producto para distinguirlo por si se repite el nombre
	 * @param peso        del producto en kg
	 * @param precio      del producto en �
	 * @param existencias que tiene cada producto
	 */
	public Productos(String nombre, String tipo, double peso, double precio, int existencias) {
		this.nombre = nombre;
		this.tipo = tipo;
		this.peso = peso;
		this.precio = precio;
		this.existencias = existencias;
	}

	/**
	 * solo nombre y tipo para usarlo en sugerencias sugerencias solo pide estos dos
	 * parametros
	 */
	public Productos() {
		this.nombre = "";
		this.tipo = "";
	}

	/**
	 * 
	 * @return nombre del producto
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * 
	 * @param nombre del producto
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * 
	 * @return tipo del producto
	 */
	public String getTipo() {
		return tipo;
	}

	/**
	 * 
	 * @param tipo del producto
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	/**
	 * 
	 * @return peso del producto
	 */
	public double getPeso() {
		return peso;
	}

	/**
	 * 
	 * @param peso del producto
	 */
	public void setPeso(double peso) {
		this.peso = peso;
	}

	/**
	 * 
	 * @return precio del producto
	 */
	public double getPrecio() {
		return precio;
	}

	/**
	 * 
	 * @param precio del producto
	 */
	public void setPrecio(double precio) {
		this.precio = precio;
	}

	/**
	 * 
	 * @return existencias del producto
	 */
	public int getExistencias() {
		return existencias;
	}

	/**
	 * 
	 * @param existencias del producto
	 */
	public void setExistencias(int existencias) {
		this.existencias = existencias;
	}

	/**
	 * juntar nombre y tipo para que salgan juntos al hacer uso de toString
	 */
	@Override
	public String toString() {
		return nombre + " " + tipo + ", peso:" + peso + "kg, precio:" + precio + "�, n�mero de existencias:"
				+ existencias;
	}
}