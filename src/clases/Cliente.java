package clases;

/**
 * clase Cliente con sus atributos, constructor, getter y setter y toString
 * 
 * @author PATRICIA.ALCALA.ALCOBER
 *
 */

public class Cliente {
	/**
	 * atributos
	 */
	private String nombre;
	private String apellido;
	private String dni;
	private int edad;
	private int numsocio;

	/**
	 * constructor vacio para rellenarlo mediante scanner
	 */
	public Cliente() {
		super();
		this.nombre = "";
		this.apellido = "";
		this.dni = "";
		this.edad = 0;
		this.numsocio = 0;
	}

	/**
	 * 
	 * @return nombre que introduzca el cliente
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * 
	 * @param nombre del cliente que sera introducido por teclado
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * 
	 * @return apellido que introduzca el cliente
	 */
	public String getApellido() {
		return apellido;
	}

	/**
	 * 
	 * @param apellido del cliente que sera introducido por teclado
	 */
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	/**
	 * 
	 * @return dni que introduzca el cliente
	 */
	public String getDni() {
		return dni;
	}

	/**
	 * 
	 * @param dni del cliente que sera introducido por teclado se validara si su
	 *            longitud es correcta y si esta escrito corretamente
	 */
	public void setDni(String dni) {
		this.dni = dni;
	}

	/**
	 * 
	 * @return edad que introduzca el cliente
	 */
	public int getEdad() {
		return edad;
	}

	/**
	 * 
	 * @param edad del cliente que sera introducido por teclado
	 */
	public void setEdad(int edad) {
		this.edad = edad;
	}

	/**
	 * 
	 * @return numsocio que sera generado aleatoriamente
	 */
	public int getNumsocio() {
		return numsocio;
	}

	/**
	 * 
	 * @param numsocio que sera generado aleatoriamente
	 */
	public void setNumsocio(int numsocio) {
		this.numsocio = numsocio;
	}

	/**
	 * metodo toString para mostrar toda la informacion junta
	 */
	@Override
	public String toString() {
		return "|Cliente " + numsocio + "| \nNombre: " + nombre + ", Apellido: " + apellido + ", DNI: " + dni
				+ ", Edad: " + edad;
	}
}