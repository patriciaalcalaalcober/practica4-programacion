package clases;

/**
 * clase Clases con sus atributos, constructor, getter y setter y toString
 * 
 * @author PATRICIA.ALCALA.ALCOBER
 *
 */

public class Clases {
	/**
	 * atributos
	 */
	private String modalidad;
	private String musica;
	private int aforo;
	private int asistentes;
	private String monitor;
	private double duracion;

	/**
	 * constructor con parametros para rellenarlo como quiera
	 * 
	 * @param modalidad  de la clase para distinguirla
	 * @param musica     de la clase que sera unica para cada tipo de clase
	 * @param aforo      de la clase
	 * @param asistentes que van a la clase
	 * @param monitor    que imparte la clase
	 * @param duracion   de la clase en horas
	 */
	public Clases(String modalidad, String musica, int aforo, int asistentes, String monitor, double duracion) {
		this.modalidad = modalidad;
		this.musica = musica;
		this.aforo = aforo;
		this.asistentes = asistentes;
		this.monitor = monitor;
		this.duracion = duracion;
	}

	/**
	 * 
	 * @return modalidad de clase
	 */
	public String getModalidad() {
		return modalidad;
	}

	/**
	 * 
	 * @param modalidad de la clase
	 */
	public void setModalidad(String modalidad) {
		this.modalidad = modalidad;
	}

	/**
	 * 
	 * @return musica que se usa
	 */
	public String getMusica() {
		return musica;
	}

	/**
	 * 
	 * @param musica de la clase
	 */
	public void setMusica(String musica) {
		this.musica = musica;
	}

	/**
	 * 
	 * @return aforo de la clase
	 */
	public int getAforo() {
		return aforo;
	}

	/**
	 * 
	 * @param aforo de cada clase
	 */
	public void setAforo(int aforo) {
		this.aforo = aforo;
	}

	/**
	 * 
	 * @return numero de asistentas
	 */
	public int getAsistentes() {
		return asistentes;
	}

	/**
	 * 
	 * @param asistentes que asisten
	 */
	public void setAsistentes(int asistentes) {
		this.asistentes = asistentes;
	}

	/**
	 * 
	 * @return monitor que da la clase
	 */
	public String getMonitor() {
		return monitor;
	}

	/**
	 * 
	 * @param monitor que imparte la clase
	 */
	public void setMonitor(String monitor) {
		this.monitor = monitor;
	}

	/**
	 * 
	 * @return duracion de la clase
	 */
	public double getDuracion() {
		return duracion;
	}

	/**
	 * 
	 * @param duracion de la clase
	 */
	public void setDuracion(double duracion) {
		this.duracion = duracion;
	}

	/**
	 * 
	 * @param asistentes de cada clase
	 * @param aforo      de cada clase
	 * @return true si hay aforo completo - false si no hay aforo completo
	 */
	public boolean aforoCompleto(int asistentes, int aforo) {
		if (asistentes < aforo) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Clase: modalidad:" + modalidad + ", musica:" + musica + ", asistentes:" + asistentes + ", monitor:"
				+ monitor + ", duracion:" + duracion + " h";
	}

}