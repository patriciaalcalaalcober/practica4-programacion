package programa;

import java.util.Scanner;

import clases.Clases;
import clases.Gimnasio;
import clases.Productos;

/**
 * Programa principal
 * 
 * @author PATRICIA.ALCALA.ALCOBER
 *
 */

public class Programa {
	static Scanner teclado = new Scanner(System.in);

	/**
	 * final static para que siempre sea esa valor
	 */
	final static double ENVIO = 3.50;

	public static void main(String[] args) {
		int opcion = 0;

		/**
		 * creacion de los objetos que reciben las cosas
		 */
		Gimnasio gimnasio = new Gimnasio(7);
		Gimnasio producto = new Gimnasio(20);

		/**
		 * ceacion a mano de los objetos de la clase Clases porque no quiero que cambie
		 * su informacion
		 */
		Clases yoga = new Clases("yoga", "relajante", 10, 8, "Alicia", 1);
		Clases zumba = new Clases("zumba", "latina", 32, 32, "Pablo", 1.45);

		/**
		 * ceacion a mano de los objetos de la clase Productos porque no quiero que
		 * cambie su informacion
		 */
		Productos producto1 = new Productos("Prote�na", "normal", 1, 24.99, 10);
		Productos producto2 = new Productos("Prote�na", "sabor a chocolate", 0.5, 25.10, 1);
		Productos producto3 = new Productos("Prote�na", "sabor a caf�", 0.5, 25.10, 7);
		Productos producto4 = new Productos("Creat�na", "monohidratada", 1, 36.40, 5);
		Productos producto5 = new Productos("C�psulas", "preentreno", 1, 48.99, 0);

		do {
			System.out.println();
			System.out.println("        Bienvenido a nuestro gimnasio         ");

			System.out.println("-----------------------------------------------");
			System.out.println("|     1. Dar de alta a un nuevo cliente       |");
			System.out.println("|                                             |");
			System.out.println("|     2. Buscar a un cliente                  |");
			System.out.println("|                                             |");
			System.out.println("|     3. Dar de baja a un cliente             |");
			System.out.println("|                                             |");
			System.out.println("|     4. Consultar lista de clientes          |");
			System.out.println("|                                             |");
			System.out.println("|     5. Cambiar los datos de un cliente      |");
			System.out.println("|                                             |");
			System.out.println("|     6. Consultar las clases que hay         |");
			System.out.println("|                                             |");
			System.out.println("|     7. Mostrar cat�logo                     |");
			System.out.println("|                                             |");
			System.out.println("|     8. Comprar producto                     |");
			System.out.println("|                                             |");
			System.out.println("|     9. Petici�n de productos                |");
			System.out.println("|                                             |");
			System.out.println("|     10. Contacto                            |");
			System.out.println("|                                             |");
			System.out.println("|     11. Salir                               |");
			System.out.println("-----------------------------------------------");
			System.out.println("\n");
			System.out.println("�Qu� desea hacer?");
			opcion = teclado.nextInt();

			teclado.nextLine();

			switch (opcion) {
			case 1:
				gimnasio.altaCliente();
				break;

			case 2:
				System.out.println("Escriba el numero de socio que desee buscar");
				int num = teclado.nextInt();
				System.out.println(gimnasio.buscarCliente(num));

				break;

			case 3:
				System.out.println("Escriba el numero de socio que desee eliminar");
				int nums = teclado.nextInt();
				gimnasio.eliminarCliente(nums);
				gimnasio.listaGeneral();
				break;

			case 4:
				gimnasio.listaGeneral();
				break;

			case 5:

				System.out.println("�Qu� desea cambiar?");
				System.out.println("1. DNI 2. Edad 3. Ambas");
				int opcion2 = teclado.nextInt();
				switch (opcion2) {
				case 1:
					System.out.println("Escriba el n�mero de socio a modificar");
					int nums1 = teclado.nextInt();
					teclado.nextLine();
					System.out.println("Escriba su nuevo DNI");
					String dni2 = teclado.nextLine();
					if ((dni2.length() == 9) && (dni2.charAt(0) >= '0' && dni2.charAt(0) <= '9')
							&& (dni2.charAt(1) >= '0' && dni2.charAt(1) <= '9')
							&& (dni2.charAt(2) >= '0' && dni2.charAt(2) <= '9')
							&& (dni2.charAt(3) >= '0' && dni2.charAt(3) <= '9')
							&& (dni2.charAt(4) >= '0' && dni2.charAt(4) <= '9')
							&& (dni2.charAt(5) >= '0' && dni2.charAt(5) <= '9')
							&& (dni2.charAt(6) >= '0' && dni2.charAt(6) <= '9')
							&& (dni2.charAt(7) >= '0' && dni2.charAt(7) <= '9')
							&& (dni2.charAt(8) >= 65 && dni2.charAt(8) <= 90)) {
						System.out.println("DNI admitido");
					} else {
						do {
							System.out.println("DNI err�neo, por favor vuelva a introducirlo");
							dni2 = teclado.nextLine();
						} while (dni2.length() != 9);
						System.out.println("DNI admitido");
					}

					gimnasio.cambiarDatosCliente(nums1, dni2);
					System.out.println("Dato actualizado exitosamente");
					gimnasio.listaGeneral();

					break;

				case 2:
					System.out.println("Escriba el n�mero de socio a modificar");
					int nums2 = teclado.nextInt();
					System.out.println("Escriba la edad a actualizar");
					int edad2 = teclado.nextInt();
					gimnasio.cambiarDatosCliente(nums2, edad2);
					System.out.println("Dato actualizado exitosamente");
					gimnasio.listaGeneral();
					break;

				case 3:
					System.out.println("Escriba el n�mero de socio a modificar");
					int nums3 = teclado.nextInt();
					teclado.nextLine();
					System.out.println("Escriba su nuevo DNI");
					String dni3 = teclado.nextLine();
					if ((dni3.length() == 9) && (dni3.charAt(0) >= '0' && dni3.charAt(0) <= '9')
							&& (dni3.charAt(1) >= '0' && dni3.charAt(1) <= '9')
							&& (dni3.charAt(2) >= '0' && dni3.charAt(2) <= '9')
							&& (dni3.charAt(3) >= '0' && dni3.charAt(3) <= '9')
							&& (dni3.charAt(4) >= '0' && dni3.charAt(4) <= '9')
							&& (dni3.charAt(5) >= '0' && dni3.charAt(5) <= '9')
							&& (dni3.charAt(6) >= '0' && dni3.charAt(6) <= '9')
							&& (dni3.charAt(7) >= '0' && dni3.charAt(7) <= '9')
							&& (dni3.charAt(8) >= 65 && dni3.charAt(8) <= 90)) {
						System.out.println("DNI admitido");
					} else {
						do {
							System.out.println("DNI err�neo, por favor vuelva a introducirlo");
							dni3 = teclado.nextLine();
						} while (dni3.length() != 9);
						System.out.println("DNI admitido");
					}

					System.out.println("Escriba la edad a actualizar");
					int edad3 = teclado.nextInt();

					gimnasio.cambiarDatosCliente(nums3, dni3, edad3);
					System.out.println("Datos actualizados exitosamente");
					gimnasio.listaGeneral();

					break;
				}

				if (opcion2 > 3) {
					System.out.println("Lo sentimos, esa opci�n no figura en nuestra base");
				}

				break;

			case 6:

				mostrarClases(yoga, zumba);
				break;

			case 7:

				mostrarCatalogo(producto1, producto2, producto3, producto4, producto5);
				break;

			case 8:
				mostrarCatalogo(producto1, producto2, producto3, producto4, producto5);
				System.out.println();
				System.out.println("�Qu� producto desea comprar?");
				int opcion3 = teclado.nextInt();
				teclado.nextLine();
				switch (opcion3) {
				case 1:
					System.out.println("Has elegido prote�na normal");
					System.out.println("N�mero de existencias: " + producto1.getExistencias());
					System.out.println();
					System.out.println("�Est� seguro de continuar con la compra? Si/No");
					String respuesta = teclado.nextLine();
					if (respuesta.equalsIgnoreCase("Si")) {
						if (producto1.getExistencias() > 0) {
							System.out.println("Producto a�adido al carrito");
							existenciasONo(opcion3, producto1, producto2, producto3, producto4, producto5);
							System.out.println();
							System.out.println(
									"�Desea recogida gratuita en tienda o env�o a domicilio con costes adicionales de 3.50�?");
							String respuesta2 = teclado.nextLine();
							if (respuesta2.equalsIgnoreCase("envio")) {
								System.out.println("Su precio final de compra asciende a: "
										+ (producto1.getPrecio() + ENVIO) + " �");
							} else if (respuesta2.equalsIgnoreCase("tienda")) {
								System.out.println(
										"�Perfecto! Puede venir a recoger su producto a nuestro establecimiento cuando desee");
							}
						} else if (producto1.getExistencias() == 0) {
							System.out.println("�Producto agotado!");
						}
					}
					if (respuesta.equalsIgnoreCase("no")) {
						System.out.println("Esperamos verle pronto");
					}
					break;

				case 2:
					System.out.println("Has elegido prote�na sabor a chocolate");
					System.out.println("N�mero de existencias: " + producto2.getExistencias());
					System.out.println();
					System.out.println("�Est� seguro de continuar con la compra? Si/No");
					String respuesta3 = teclado.nextLine();
					if (respuesta3.equalsIgnoreCase("Si")) {
						if (producto2.getExistencias() > 0) {
							System.out.println("Producto a�adido al carrito");
							existenciasONo(opcion3, producto1, producto2, producto3, producto4, producto5);
							System.out.println();
							System.out.println(
									"�Desea recogida gratuita en tienda o env�o a domicilio con costes adicionales de 3.50�?");
							String respuesta4 = teclado.nextLine();
							if (respuesta4.equalsIgnoreCase("envio")) {
								System.out.println("Su precio final de compra asciende a: "
										+ (producto2.getPrecio() + ENVIO) + " �");
							} else if (respuesta4.equalsIgnoreCase("tienda")) {
								System.out.println(
										"�Perfecto! Puede venir a recoger su producto a nuestro establecimiento cuando desee");
							}
						} else if (producto2.getExistencias() == 0) {
							System.out.println("�Producto agotado!");
						}
					}
					if (respuesta3.equalsIgnoreCase("no")) {
						System.out.println("Esperamos verle pronto");
					}
					break;

				case 3:
					System.out.println("Has elegido prote�na sabor a caf�");
					System.out.println("N�mero de existencias: " + producto3.getExistencias());
					System.out.println();
					System.out.println("�Est� seguro de continuar con la compra? Si/No");
					String respuesta5 = teclado.nextLine();
					if (respuesta5.equalsIgnoreCase("Si")) {

						if (producto3.getExistencias() > 0) {
							System.out.println("Producto a�adido al carrito");
							existenciasONo(opcion3, producto1, producto2, producto3, producto4, producto5);
							System.out.println();
							System.out.println(
									"�Desea recogida gratuita en tienda o env�o a domicilio con costes adicionales de 3.50�?");
							String respuesta6 = teclado.nextLine();
							if (respuesta6.equalsIgnoreCase("envio")) {
								System.out.println("Su precio final de compra asciende a: "
										+ (producto3.getPrecio() + ENVIO) + " �");
							} else if (respuesta6.equalsIgnoreCase("tienda")) {
								System.out.println(
										"�Perfecto! Puede venir a recoger su producto a nuestro establecimiento cuando desee");
							}
						} else if (producto3.getExistencias() == 0) {
							System.out.println("�Producto agotado!");
						}
					}
					if (respuesta5.equalsIgnoreCase("no")) {
						System.out.println("Esperamos verle pronto");
					}
					break;

				case 4:
					System.out.println("Has elegido creat�na monohidratada");
					System.out.println("N�mero de existencias: " + producto4.getExistencias());
					System.out.println();
					System.out.println("�Est� seguro de continuar con la compra? Si/No");
					String respuesta7 = teclado.nextLine();
					if (respuesta7.equalsIgnoreCase("Si")) {

						if (producto4.getExistencias() > 0) {
							System.out.println("Producto a�adido al carrito");
							existenciasONo(opcion3, producto1, producto2, producto3, producto4, producto5);
							System.out.println();
							System.out.println(
									"�Desea recogida gratuita en tienda o env�o a domicilio con costes adicionales de 3.50�?");
							String respuesta8 = teclado.nextLine();
							if (respuesta8.equalsIgnoreCase("envio")) {
								System.out.println("Su precio final de compra asciende a: "
										+ (producto4.getPrecio() + ENVIO) + " �");
							} else if (respuesta8.equalsIgnoreCase("tienda")) {
								System.out.println(
										"�Perfecto! Puede venir a recoger su producto a nuestro establecimiento cuando desee");
							}
						} else if (producto4.getExistencias() == 0) {
							System.out.println("�Producto agotado!");
						}
					}
					if (respuesta7.equalsIgnoreCase("no")) {
						System.out.println("Esperamos verle pronto");
					}
					break;

				case 5:
					System.out.println("Has elegido c�psula preentreno");
					System.out.println("N�mero de existencias: " + producto5.getExistencias());
					System.out.println();
					System.out.println("�Est� seguro de continuar con la compra? Si/No");
					String respuesta9 = teclado.nextLine();
					if (respuesta9.equalsIgnoreCase("Si")) {
						if (producto5.getExistencias() > 0) {
							System.out.println("Producto a�adido al carrito");
							existenciasONo(opcion3, producto1, producto2, producto3, producto4, producto5);
							System.out.println();
							System.out.println(
									"�Desea recogida gratuita en tienda o env�o a domicilio con costes adicionales de 3.50�?");
							String respuesta10 = teclado.nextLine();
							if (respuesta10.equalsIgnoreCase("envio")) {
								System.out.println("Su precio final de compra asciende a: "
										+ (producto5.getPrecio() + ENVIO) + " �");
							} else if (respuesta10.equalsIgnoreCase("tienda")) {
								System.out.println(
										"�Perfecto! Puede venir a recoger su producto a nuestro establecimiento cuando desee");
							}
						} else if (producto5.getExistencias() == 0) {
							System.out.println("�Producto agotado!");
						}
					}
					if (respuesta9.equalsIgnoreCase("no")) {
						System.out.println("Esperamos verle pronto");
					}
					break;

				}
				if (opcion3 > 5) {
					System.out.println("Lo sentimos, esa opci�n no figura en nuestra base");
				}
				break;

			case 9:
				producto.sugerencias();
				break;

			case 10:
				contactar();
				break;

			case 11:
				System.out.println("Esperamos volver a verlo pronto");
				break;
			}

			if (opcion > 11) {
				System.out.println("Lo sentimos, esa funci�n no figura en nuestro sistema. \nPruebe de nuevo");
			}

		} while (opcion != 11);

		teclado.close();

	}

	/**
	 * metodo que muestra la informacion de los objetos creados de la clase Clases
	 * 
	 * @param yoga  para obtener los datos del objeto
	 * @param zumba para obtener los datos del objeto
	 */
	public static void mostrarClases(Clases yoga, Clases zumba) {
		System.out.println(yoga.toString());
		System.out.println("Aforo completo:");
		if (yoga.aforoCompleto(8, 10)) {
			System.out.println("S�");
		} else {
			System.out.println("No");
		}
		System.out.println();
		System.out.println(zumba.toString());
		System.out.println("Aforo completo:");
		if (yoga.aforoCompleto(32, 32)) {
			System.out.println("S�");
		} else {
			System.out.println("No");
		}
	}

	/**
	 * metodo que muestra el catalogo que contiene todos los objetos creados de la
	 * clase Productos
	 * 
	 * @param producto1 para obtener los datos del objeto y mostrarlo con toString
	 * @param producto2 para obtener los datos del objeto y mostrarlo con toString
	 * @param producto3 para obtener los datos del objeto y mostrarlo con toString
	 * @param producto4 para obtener los datos del objeto y mostrarlo con toString
	 * @param producto5 para obtener los datos del objeto y mostrarlo con toString
	 */
	public static void mostrarCatalogo(Productos producto1, Productos producto2, Productos producto3,
			Productos producto4, Productos producto5) {
		System.out.println("Bienvenido a nuestro cat�logo");
		System.out.println();
		System.out.println("Nuestros productos son: ");
		System.out.println("1. " + producto1);
		System.out.println();
		System.out.println("2. " + producto2);
		System.out.println();
		System.out.println("3. " + producto3);
		System.out.println();
		System.out.println("4. " + producto4);
		System.out.println();
		System.out.println("5. " + producto5);
	}

	/**
	 * metodo que va restando el numero de existencias en cada objeto cuando es
	 * seleccionado
	 * 
	 * @param opcion3   para pasar la opcion elegida del menu principal y se meta
	 *                  automaticamente en el menu del metodo
	 * @param producto1 para recibir los parametros
	 * @param producto2 para recibir los parametros
	 * @param producto3 para recibir los parametros
	 * @param producto4 para recibir los parametros
	 * @param producto5 para recibir los parametros
	 */
	public static void existenciasONo(int opcion3, Productos producto1, Productos producto2, Productos producto3,
			Productos producto4, Productos producto5) {

		switch (opcion3) {
		case 1:
			for (int i = producto1.getExistencias(); i >= 0; i--) {
				if (producto1.getExistencias() > 0) {
					producto1.setExistencias(producto1.getExistencias() - 1);
					System.out.println("N�mero de existencias tras su elecci�n: " + producto1.getExistencias());
					break;
				}
			}

			break;

		case 2:
			for (int i = producto2.getExistencias(); i >= 0; i--) {
				if (producto2.getExistencias() > 0) {
					producto2.setExistencias(producto2.getExistencias() - 1);
					System.out.println("N�mero de existencias tras su elecci�n: " + producto2.getExistencias());

					break;
				}
			}
			break;

		case 3:
			for (int i = producto3.getExistencias() - 1; i > 0; i--) {
				if (producto3.getExistencias() > 0) {
					producto3.setExistencias(producto3.getExistencias() - 1);
					System.out.println("N�mero de existencias tras su elecci�n: " + producto3.getExistencias());
					break;
				}
			}
			break;

		case 4:
			for (int i = producto4.getExistencias() - 1; i > 0; i--) {
				if (producto4.getExistencias() > 0) {
					producto4.setExistencias(producto4.getExistencias() - 1);
					System.out.println("N�mero de existencias: " + producto4.getExistencias());
					break;
				}

			}
			break;

		case 5:
			for (int i = producto5.getExistencias() - 1; i > 0; i--) {
				if (producto5.getExistencias() > 0) {
					producto5.setExistencias(producto5.getExistencias() - 1);
					System.out.println("N�mero de existencias tras su elecci�n: " + producto5.getExistencias());
					break;
				}
			}
			break;
		}
	}

	/**
	 * metodo para aportar informacion al cliente
	 */
	public static void contactar() {
		System.out.println("Nuestros m�todos de contacto son:");
		System.out.println("1. Tel�fono");
		System.out.println("2. Correo electr�nico");
		System.out.println("3. Direcci�n");
		int opcion = teclado.nextInt();

		teclado.nextLine();

		switch (opcion) {
		case 1:
			System.out.println("Puede contactar con nosotros a trav�s de nuestro tel�fono 965659741");
			break;
		case 2:
			System.out.println("Puede enviarnos un mensaje a nuestro email gymgym@gmail.com (Si/No)");
			String respuesta11 = teclado.nextLine();
			if (respuesta11.equalsIgnoreCase("si")) {
				System.out.println("Escriba el mensaje que desea enviar: ");
				String mensaje = teclado.nextLine();
				System.out.println("Intentaremos responderle su mensaje -" + mensaje + "- cuanto antes");
			}
			if (respuesta11.equalsIgnoreCase("no")) {
				System.out.println("Esperamos haberle ayudado");
			}
			break;

		case 3:
			System.out.println("Puede venir a vernos cuando quieras, �te esperamos en Avenida Galaxia 9!");
			break;
		}
	}
}
